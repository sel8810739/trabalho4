from flask import Flask, request, jsonify

def fatorial(n) -> int:
    if n < 0:
        raise ValueError("Número deve ser positivo")
    output = 1
    for i in range(1, n+1, 1):
        output = i * output
    return output

app = Flask(__name__)
@app.route('/fatorial', methods=['POST'])
def get_fatorial():
    data = request.get_json()
    number = int(data['fat'])
    result = fatorial(number)
    return jsonify({'fat_result':result})

if __name__ == '__main__':
    app.run(debug=True, host= 'localhost', port= 5000)