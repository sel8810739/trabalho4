# Trabalho 4

Script elaborado para implementar uma API para a exibição do resultado do cálculo do fatorial de um número.

## Funcionamento

O trabalho contém o arquivo **main\.py**, contendo a criação de um servidor local, denominado de localhost, na porta 5000. O servidor é responsável por receber uma requisição, contendo um número inteiro. Após a requisição, o servidor utilizará a função **fatorial** para calcular o fatorial do número recebido. Ao final, o servidor retornará o resultado do fatorial do número.

## Execução

Para executar o servidor, deve executar o **main\.py** para que o servidor seja criado e espere por parâmetros. Além disso, deve-se enviar ao servidor a requisição contendo a ação que o servidor irá executar. Neste caso, a requisição é realizada através do comando **curl** contendo um JSON com os parâmetros de função **"fat"** e  número inteiro **n** e o endereço http do servidor local. O comando **curl** completo é o seguinte:

```bash
curl -X POST -H "Content-Type: application/json" -d '{"fat": 5}' http://localhost:5000/fatorial
```

Neste caso, o número inteiro é 5.

## Saída

A saída do servidor será o resultado do cálculo do fatorial do número inteiro recebido. Neste caso, o resultado será o seguinte:

```bash
{"fat_result": 120}
```

Este resultado pode ser observador no terminal onde o servidor está sendo executado.